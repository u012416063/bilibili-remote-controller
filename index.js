const path = require("path");
const ks = require('node-key-sender');
const express = require('express');
const app = express();
const port = 3000;

const actions = {
  next: () => ks.sendKey('down'),
  previous: () => ks.sendKey('up'),
  up: () => ks.sendCombination(['shift', 'up']),
  down: () => ks.sendCombination(['shift', 'down']),
  pauseOrPlay: () => ks.sendKey('space')
};

Object.keys(actions).forEach(action => {
  app.get(`/${action}`, async (req, res) => {
    try {
      await actions[action]();
      res.json({ success: true, message: `${action} action completed` });
    } catch (error) {
      res.status(500).json({ success: false, message: `Error performing ${action} action`, error });
    }
  });
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});